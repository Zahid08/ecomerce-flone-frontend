import { combineReducers } from 'redux';
import { IntlReducer as Intl, IntlProvider } from 'react-redux-multilingual'

// Import custom components
import sampleReducer from './sample-reducers';

const rootReducer = combineReducers({
    data: sampleReducer,
    Intl
});

export default rootReducer;