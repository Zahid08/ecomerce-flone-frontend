import { FETCH_SAMPLE_BEGIN } from "../constants/ActionTypes";

const initialState = {
    products: [],
};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SAMPLE_BEGIN:
            return { ...state, products: action.products };
        default:
            return state;
    }
};
export default productReducer;
