import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

// Import custom components
import { connect } from "react-redux";

class Footers extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <div>
                <footer className="footer-area bg-gray pt-100 pb-70">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-2 col-md-4 col-sm-4">
                                <div className="copyright mb-30">
                                    <div className="footer-logo">
                                        <a href="index.html">
                                            <img alt="" src="assets/img/logo/logo.png"/>
                                        </a>
                                    </div>
                                    <p>© 2019 <a href="#">Flone</a>.<br/> All Rights Reserved</p>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-4 col-sm-4">
                                <div className="footer-widget mb-30 ml-30">
                                    <div className="footer-title">
                                        <h3>ABOUT US</h3>
                                    </div>
                                    <div className="footer-list">
                                        <ul>
                                            <li><a href="about.html">About us</a></li>
                                            <li><a href="#">Store location</a></li>
                                            <li><a href="contact.html">Contact</a></li>
                                            <li><a href="#">Orders tracking</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-4 col-sm-4">
                                <div className="footer-widget mb-30 ml-50">
                                    <div className="footer-title">
                                        <h3>USEFUL LINKS</h3>
                                    </div>
                                    <div className="footer-list">
                                        <ul>
                                            <li><a href="#">Returns</a></li>
                                            <li><a href="#">Support Policy</a></li>
                                            <li><a href="#">Size guide</a></li>
                                            <li><a href="#">FAQs</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-6 col-sm-6">
                                <div className="footer-widget mb-30 ml-75">
                                    <div className="footer-title">
                                        <h3>FOLLOW US</h3>
                                    </div>
                                    <div className="footer-list">
                                        <ul>
                                            <li><a href="#">Facebook</a></li>
                                            <li><a href="#">Twitter</a></li>
                                            <li><a href="#">Instagram</a></li>
                                            <li><a href="#">Youtube</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                                <div className="footer-widget mb-30 ml-70">
                                    <div className="footer-title">
                                        <h3>SUBSCRIBE</h3>
                                    </div>
                                    <div className="subscribe-style">
                                        <p>Get E-mail updates about our latest shop and special offers.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

//If Need Any State Value Used For redux state
const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(Footers);
