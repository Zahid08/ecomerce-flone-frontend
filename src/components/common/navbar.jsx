import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

import { connect } from "react-redux";

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    render() {
        return (
            <div>
                    {/*--------------Start Top Navbar------------------*/}
                    <div className="header-top-area">
                        <div className="container">
                            <div className="header-top-wap">
                                <div className="language-currency-wrap">
                                    <div className="same-language-currency language-style">
                                        <a href="#">English <i className="fa fa-angle-down"></i></a>
                                        <div className="lang-car-dropdown">
                                            <ul>
                                                <li><a href="#">Arabic </a></li>
                                                <li><a href="#">Bangla </a></li>
                                                <li><a href="#">Hindi </a></li>
                                                <li><a href="#">Spanish </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="same-language-currency use-style">
                                        <a href="#">USD <i className="fa fa-angle-down"></i></a>
                                        <div className="lang-car-dropdown">
                                            <ul>
                                                <li><a href="#">Taka (BDT) </a></li>
                                                <li><a href="#">Riyal (SAR) </a></li>
                                                <li><a href="#">Rupee (INR) </a></li>
                                                <li><a href="#">Dirham (AED) </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="same-language-currency">
                                        <p>Call Us 3965410</p>
                                    </div>
                                </div>
                                <div className="header-offer">
                                    <p>Free delivery on order over <span>$200</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*--------------End Top Navbar------------------*/}
            </div>
        );
    }
}

//If Need Any State Value Used For redux state
const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(Navbar);
