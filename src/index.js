import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ScrollContext } from "react-router-scroll-4";
import {IntlProvider } from "react-redux-multilingual";

//Layouts Export
import Fashion from "./components/layouts/fashion/fashion-main";

//Import Custom Components
import store from "./store";
import Layout from "./components/app";
import testComponet from "./components/pages/testComponet";

//Collection of pages
class Root extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            SomethingsDemo: []
        };
    }
    render(){
        let layout='fashion';
        let mainLayoutRender;
        if (layout=='fashion'){
            mainLayoutRender= <Route exact path={`${process.env.PUBLIC_URL}/`} component={Fashion} />;
        }
        return(
            <Provider store={store}>
                <IntlProvider>
                    <BrowserRouter basename={"/"}>
                        <ScrollContext>
                            <Switch>
                                {mainLayoutRender}
                                <Layout>
                                    <Route path={`${process.env.PUBLIC_URL}/test`} component={testComponet} />
                                </Layout>
                            </Switch>
                        </ScrollContext>
                    </BrowserRouter>
                </IntlProvider>
            </Provider>
        );
    }
}

ReactDOM.render(<Root/>, document.getElementById('root'));

